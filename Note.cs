using System;
using System.Collections.Generic;
using System.Text;

namespace LABV1
{
    class Note
    {
        private string mText;
        private string mAuthor;
        private int mPriority;

        public Note()
        {
            mText = "";
            mAuthor = "Unknown";
            mPriority = 0;
        }
        public Note(string author)
        {
            mAuthor = author;
        }

        public Note(string author, string text, int priority)
        {
            mAuthor = author;
            mText = text;
            mPriority = priority;
        }

        public void setText(string text)
        {
            mText = text;
        }
        public void setPriority(int priority)
        {
            mPriority = priority;
        }
        private void setAuthor(string author)
        {
            mAuthor = author;
        }
        public string getText()
        {
            return mText;
        }
        public string getAuthor()
        {
            return mAuthor;
        }
        public int getPriority()
        {
            return mPriority;
        }
        public string Text { get { return mText; } set { mText = value; } }
        public int Priority { get { return mPriority; } set { mPriority = value; } }
        public string Author { get { return mAuthor; } private set { mAuthor = value; } }

        public override string ToString()
        {
            return Priority + ", " + Text + ", " + Author;
        }
    }
}
