using System;
using System.Collections.Generic;
using System.Text;

namespace LABV1
{
    class Entry
    {

        static void Main(string[] args)
        {
            Note note = new Note("Petar");
            Note secondnote = new Note("Stjepan", "Sreca prati hrabre", 0);
            Note thridnote = new Note();
            TimeStamp timenote = new TimeStamp("Magdalena", "Hej ljudi", 0, DateTime.Now);
            TimeStamp changeablenote = new TimeStamp("Bruno", "Pametnica", 1, new DateTime(1999, 4, 25, 6, 50, 30));
            ToDoList tasklist = new ToDoList();

            
            note.setText("Dobar dan");
            note.setPriority(2);

            thridnote.Text = "Sladak je svijet.";
            thridnote.Priority = 1;
          

            Console.WriteLine(note.getPriority() + ", " + note.getText() + ", " + note.getAuthor());

            Console.WriteLine(secondnote.ToString());

            Console.WriteLine(thridnote.ToString());
            
            Console.WriteLine(timenote.ToString());
            
            Console.WriteLine(changeablenote.ToString());

            for(int i = 0; i < 3; i++)
            {
                string author = Console.ReadLine();
                string text = Console.ReadLine();
                int priority;

                int.TryParse(Console.ReadLine(), out priority);
                tasklist.addNote(new Note(author, text, priority));

            }

            Console.WriteLine(tasklist.ToString());

            tasklist.finishedTasks();
            
            Console.WriteLine(tasklist.ToString());
            
        }

    }
}
