using System;
using System.Collections.Generic;
using System.Text;

namespace LABV1
{
    class ToDoList
    {
        private List<Note> mTasks = new List<Note>();

        public ToDoList() { }

        public void addNote(Note note)
        {
            mTasks.Add(note);
        }

        public void removeNote(Note note)
        {
            mTasks.Remove(note);
        }

        public Note getNote(int index)
        {
            return mTasks[index];
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for(int i = 0; i < mTasks.Count; i++)
            {
                stringBuilder.Append(mTasks[i]).Append("\n");
            }
            return stringBuilder.ToString();
        }

        public void finishedTasks()
        {
            int maxPriority = getMaxPriority();
            for(int i = 0; i < mTasks.Count; i++)
            {
                if(mTasks[i].Priority >= maxPriority)
                {
                    removeNote(mTasks[i]);
                }
            }
        }

        public int getMaxPriority()
        {
            int maximum = 0;
            for(int i = 0; i < mTasks.Count; i++)
            {
                if(maximum < mTasks[i].Priority)
                {
                    maximum = mTasks[i].Priority;
                }
            }
            return maximum;
        }
    }
}
