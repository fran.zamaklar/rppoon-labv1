using System;
using System.Collections.Generic;
using System.Text;

namespace LABV1
{
    class TimeStamp : Note
    {
        public DateTime Time { get; set;  }


        public TimeStamp(string author, string text, int priority, DateTime time) : base(author, text, priority)
        {

            Time = time;
        }
     
        public override string ToString()
        {
            return base.ToString() + ", " + Time;
        }

    }
}
